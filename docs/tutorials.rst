************
🚀 Tutorials
************


Jupyter + EnOSlib + Grid'5000 = 💖

This section is a work in progress.
Be adventurous, if you could contribute (feedback, pr ...) that would be
amazingly appreciated :)


.. toctree::
    :maxdepth: 1
    :caption: Contents:

    jupyter/00_setup_and_basics.ipynb
    jupyter/01_remote_actions_and_variables.ipynb
    jupyter/02_observability.ipynb
    jupyter/03_using_several_networks.ipynb
    jupyter/04_working_with_virtualized_resources.ipynb